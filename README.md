# Multipage form

A form displayed by section in multiple steps.
It's a personal adaptation of what Ibrahima did on https://www.ibrahima-ndaw.com/fr/blog/awesome-stepper-form/ 

it's displayed in a MVC structure implemented with Codeigniter3.

to open it, just copy the files on your local server and type in your browser: http://localhost/multipage-form/index.php/Accueil