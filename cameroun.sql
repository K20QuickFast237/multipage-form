-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mer. 20 oct. 2021 à 16:59
-- Version du serveur : 10.4.21-MariaDB
-- Version de PHP : 7.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `cameroun`
--

-- --------------------------------------------------------

--
-- Structure de la table `departement`
--

CREATE TABLE `departement` (
  `id` int(5) NOT NULL,
  `id_region` int(5) NOT NULL,
  `nom` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `departement`
--

INSERT INTO `departement` (`id`, `id_region`, `nom`) VALUES
(1, 1, 'Wouri'),
(2, 1, 'Sanaga-Maritime'),
(3, 1, 'Moungo\r\n'),
(4, 1, 'Nkam'),
(9, 2, 'Diamaré'),
(10, 2, 'Logone-et-Chari'),
(11, 2, 'Mayo-Danay'),
(12, 2, 'Mayo-Kani'),
(13, 2, 'Mayo-Sava'),
(14, 2, 'Mayo-Tsanaga'),
(15, 3, 'Bénoué'),
(16, 3, 'Faro'),
(17, 3, 'Mayo-Louti'),
(18, 3, 'Mayo-Rey'),
(19, 4, 'Djérem'),
(20, 4, 'Faro-et-Déo'),
(21, 4, 'Mayo-Banyo'),
(22, 4, 'Mbéré	Meiganga\r\n'),
(23, 4, 'Vina'),
(24, 5, 'Haute-Sanaga'),
(25, 5, 'Lekié'),
(26, 5, 'Mbam-et-Inoubou'),
(27, 5, 'Mbam-et-Kim'),
(28, 5, 'Méfou-et-Afamba	'),
(29, 5, 'Méfou-et-Akono'),
(30, 5, 'Mfoundi'),
(31, 5, 'Nyong-et-Kellé'),
(32, 5, 'Nyong-et-Mfoumou'),
(33, 5, 'Nyong-et-So’o'),
(34, 6, 'Bamboutos'),
(35, 6, 'Haut-Nkam'),
(36, 6, 'Hauts-Plateaux'),
(37, 6, 'Koung-Khi'),
(38, 6, 'Menoua'),
(39, 6, 'Mifi'),
(40, 6, 'Ndé'),
(41, 6, 'Noun'),
(42, 7, 'Boyo'),
(43, 7, 'Bui'),
(44, 7, 'Donga-Mantung'),
(45, 7, 'Menchum'),
(46, 7, 'Mezam'),
(47, 7, 'Momo'),
(48, 7, 'Ngo-Ketunjia'),
(49, 8, 'Fako'),
(50, 8, 'Koupé-Manengouba'),
(51, 8, 'Lebialem'),
(52, 8, 'Manyu'),
(53, 8, 'Meme'),
(54, 8, 'Ndian'),
(55, 9, 'Dja-et-Lobo'),
(56, 9, 'Mvila'),
(57, 9, 'Océan'),
(58, 9, 'Vallée-du-Ntem'),
(59, 10, 'Boumba-et-Ngoko'),
(60, 10, 'Haut-Nyong'),
(61, 10, 'Kadey'),
(62, 10, 'Lom-et-Djérem');

-- --------------------------------------------------------

--
-- Structure de la table `region`
--

CREATE TABLE `region` (
  `id` int(5) NOT NULL,
  `nom` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `region`
--

INSERT INTO `region` (`id`, `nom`) VALUES
(1, 'Littoral'),
(2, 'Extreme Nord'),
(3, 'Nord'),
(4, 'Adamawa'),
(5, 'Centre'),
(6, 'Ouest'),
(7, 'Nord Ouest'),
(8, 'Sud Ouest'),
(9, 'Sud'),
(10, 'Est');

-- --------------------------------------------------------

--
-- Structure de la table `ville`
--

CREATE TABLE `ville` (
  `id` int(5) NOT NULL,
  `id_departement` int(5) NOT NULL,
  `nom` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ville`
--

INSERT INTO `ville` (`id`, `id_departement`, `nom`) VALUES
(1, 1, 'wouri 1'),
(2, 1, 'wouri 2'),
(3, 1, 'wouri 3'),
(4, 2, 'Sanaga-Maritime 1'),
(5, 2, 'sanaga-Maritime 2'),
(6, 2, 'sanaga-Maritime 3'),
(7, 3, 'Moungo 1'),
(8, 3, 'Moungo 2'),
(9, 3, 'Moungo 3'),
(10, 4, 'Nkam 1'),
(11, 4, 'Nkam 2'),
(12, 4, 'Nkam 3'),
(13, 9, 'Diamaré 1'),
(14, 9, 'Diamaré é'),
(15, 9, 'Diamaré 3'),
(16, 10, 'Logone-et-Chari 1'),
(17, 10, 'Logone-et-Chari 2'),
(18, 10, 'Logone-et-Chari 3'),
(19, 11, 'Mayo-Danay 1'),
(20, 11, 'Mayo-Danay 2'),
(21, 11, 'Mayo-Danay'),
(22, 12, 'Mayo-Kani 1\r\n'),
(23, 12, 'Mayo-Kani 2'),
(24, 12, 'Mayo-Kani 3'),
(25, 13, 'Mayo-Sava 1'),
(26, 13, 'Maoyo-Sava 2'),
(27, 13, 'Mayo-Sava 3'),
(28, 14, 'Mayo-Tsanaga 1'),
(29, 14, 'Mayo-Tsanaga 2'),
(30, 14, 'Mayo-Tsanaga 3'),
(31, 15, 'Bénoué 1'),
(32, 15, 'Benoue 2'),
(33, 16, 'Faro 1'),
(34, 16, 'Faro 2'),
(35, 17, 'Mayo-Louti 1'),
(36, 17, 'Mayo-Louti 2'),
(37, 18, 'Mayo-Rey 1'),
(38, 18, 'Mayo-Rey 2'),
(39, 19, 'Djérem 1'),
(40, 19, 'Djerem 2'),
(41, 20, 'Faro-et-Déo 1'),
(42, 20, 'Faro-et-Deo 2'),
(43, 21, 'Mayo-Banyo'),
(44, 21, 'Mayo-Banyo');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `departement`
--
ALTER TABLE `departement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dept_region_fk` (`id_region`);

--
-- Index pour la table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `ville`
--
ALTER TABLE `ville`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ville_departement_fk` (`id_departement`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `departement`
--
ALTER TABLE `departement`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT pour la table `region`
--
ALTER TABLE `region`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT pour la table `ville`
--
ALTER TABLE `ville`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `departement`
--
ALTER TABLE `departement`
  ADD CONSTRAINT `dept_region_fk` FOREIGN KEY (`id_region`) REFERENCES `region` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `ville`
--
ALTER TABLE `ville`
  ADD CONSTRAINT `ville_departement_fk` FOREIGN KEY (`id_departement`) REFERENCES `departement` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
